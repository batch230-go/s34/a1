// This will load the expressjs module into our application and save it in a variable called express
const express = require("express");
const port = 4000;

// The app is our server
// This will create an application that uses express and stores it as an app
const app = express();

// Middleware (request handlers)
// express.json() is a method which allow us to handle the streaming of data and automatically parse the incoming JSON from our request body

app.use(express.json()); // << This is a request handler converted to javascript obj


// mock data
let users = [
    {
        username: "TStark3000",
        email: "starkindustries@mail.com",
        password: "notPeterParker"
    },
    {
        username: "ThorThunder",
        email: "thorStrongestAvenger@mail.com",
        password: "iLoveStormBreaker"
    }
];


app.get("/", (request, response) => {
    response.send("Hello from my first ExpressJSAPI");
});

app.get("/greeting", (request, response) => {
    response.send("Hello from Batch230-Go");
});

// retrieval of users in mock database
app.get("/users", (request, response) => {
    response.send(users);
});

app.post("/users", (request, response) => {
    let newUser = {
        username : request.body.username,
        email: request.body.email,
        password: request.body.password
    }
    users.push(newUser);
    console.log(users);
    response.send(users);
});


// UPDATE Element Obj
app.put("/users/:index", (request, response) => {
    console.log(request.body);
    console.log(request.params);
    let index = parseInt(request.params.index);

    users[index].password = request.body.password;
    response.send(users[index]);
});


// DELETE Last Element Obj
app.delete("/users", (request, response) => {
    users.pop();
    response.send(users);
});








// Activity Part
/*
    Create a new collection in Postman called s32-activity
    Save the Postman collection in your s34 folder
*/

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
];

// [GET]
// >> Create a new route to get and send items array in the client (GET ALL ITEMS)
// Insert your code here...
app.get("/items", (request, response) => {
    response.send(items);
});



// [POST]
// >> Create a new route to create and add a new item object in the items array (CREATE ITEMS)
// >> Send the updated items array in the client
// >> Check if the post method route for our users for reference
// Insert your code here...
app.post("/items", (request, response) => {
    let newItem = {
        name : request.body.name,
        price: request.body.price,
        isActive: request.body.isActive
    }
    items.push(newItem);
    console.log(items);
    response.send(items);
});




// [PUT]
// >> Create a new route which can update the price of a single item in the array (UPDATE ITEMS)
// >> Pass the index number of the item that you want t o update in the request params (include an index number to the URL)
// >> Add the price update in the request body
// >> Reassign the new price from our request body
// >> Send the updated item to the client
// Insert your code here...
app.put("/items/:index", (request, response) => {
    console.log(request.body);
    console.log(request.params);
    let index = parseInt(request.params.index);

    items[index].price = request.body.price;
    response.send(items[index]);
});




app.listen(port, () => console.log(`Server is not running at port ${port}`));





